# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0 filetype=yaml:

{# You're looking at the template here, so you can ignore the below
   warning. This is the right file to edit #}
########################################
#                                      #
# THIS FILE IS GENERATED, DO NOT EDIT  #
# Edit .gitlab-ci/ci.template instead  #
#                                      #
# Regenerate with:
#   TEMPLATE_SHA="$(sed -n 's/^.templates_sha: *\&template_sha *\([0-9a-f]\+\) .*/\1/p' ./.gitlab-ci/ci.template)"
#   pip3 install "git+http://gitlab.freedesktop.org/freedesktop/ci-templates@$TEMPLATE_SHA"
#   ci-fairy generate-template
#
########################################


.templates_sha: &template_sha ffe4d1b10aab7534489f0c4bbc4c5899df17d3f2 # see https://docs.gitlab.com/ee/ci/yaml/#includefile

{# Group distros by their common (name,) tuples.#}
{% set distro_groups = [] %}
{% for distro in distributions %}
  {% set g = {'name':distro.name} %}
  {% if g not in distro_groups %}
  {%   do distro_groups.append(g) %}
  {% endif %}
{% endfor %}

{# The "default_distro" builds our pages and is used for check-{tree,patch} tests. It is the first distro with tier 1. #}
{% set default_distro = [] %}
{% for distro in distributions %}
  {% if distro.tier == 1 and default_distro|length == 0 %}
    {% do default_distro.append(distro) %}
  {% endif %}
{% endfor %}
{% set default_distro = default_distro[0] %}

include:
{% for distro_group in distro_groups|sort(attribute='name') %}
  # {{ distro_group.name.capitalize() }} container builder template
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/{{distro_group.name}}.yml'
{% endfor %}

stages:
  - prep
  - tier1
  - tier2
  - tier3
  - deploy
  - triage
  - container_clean

variables:
  FDO_UPSTREAM_REPO: NetworkManager/NetworkManager
  GIT_DEPTH: 1
  # These tags should be updated each time the list of packages is updated
  # changing these will force rebuilding the associated image
  # Note: these tags have no meaning and are not tied to a particular NM version
  #
  # This is done by running `ci-fairy generate-template` and possibly bumping
  # ".default_tag".
{% for distro_group in distro_groups|sort(attribute='name') %}
  {{"%-13s"| format(distro_group.name.upper() + '_TAG:')}}'tag-{{
    (ci_fairy.hashfiles('./.gitlab-ci/config.yml',
                        './.gitlab-ci/ci.template',
                        './.gitlab-ci/' + base_types[distro_group.name] + '-install.sh',
                        './contrib/' + base_types[distro_group.name] + '/REQUIRED_PACKAGES'))[0:12]
  }}'
{% endfor %}

{% for distro_group in distro_groups|sort(attribute='name') %}
  {{"%-13s"| format(distro_group.name.upper() + '_EXEC:')}}'bash .gitlab-ci/{{base_types[distro_group.name]}}-install.sh'
{% endfor %}

.nm_artifacts:
  variables:
    NM_BUILD_TARBALL: 1
  artifacts:
    expire_in: 5 days
    when: always
    paths:
      - docs-html
      - NetworkManager-1*.tar.xz
      - NetworkManager-1*.src.rpm
      - nm-test.log

.nm_artifacts_debug:
  artifacts:
    expire_in: 5 days
    when: always
    paths:
      - nm-test.log

#################################################################
#                                                               #
#                     containers stage                          #
#                                                               #
#################################################################

# Build a container for each distribution + version. The ci-templates
# will re-use the containers if the tag doesn't change.
{% for distro in distributions %}
{% for version in distro.versions %}

tier{{distro.tier}}:{{distro.name}}:{{version}}@prep:
  extends:
    - .fdo.container-build@{{distro.name}}
  stage: prep
  variables:
    GIT_STRATEGY: none
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
    FDO_DISTRIBUTION_EXEC: ${{distro.name.upper()}}_EXEC
{% if distro.tier > 1 %}
  when: manual
{% endif %}
{% endfor %}
{% endfor %}

#################################################################
#                                                               #
#                   container clean stage                       #
#                 run during the clean stage                    #
#                                                               #
#################################################################

#
# This stage will look for the container images we currently have in
# the registry and will remove any that are not tagged with the provided
# $container_image:$tag
#
# This job only runs for a scheduled pipeline.
#
# Go to your Profile, Settings, Access Tokens
# Create a personal token with 'api' scope, copy the value.
# Go to CI/CD, Schedules, schedule a monthly job.
# Define a variable of type File named AUTHFILE. Content is that token
# value.
.container-clean:
  stage: container_clean
  image: golang:alpine
  before_script:
    - apk add python3 py-pip git
    - pip3 install git+http://gitlab.freedesktop.org/freedesktop/ci-templates
  script:
    - ci-fairy -v --authfile $AUTHFILE delete-image
            --repository $FDO_DISTRIBUTION_NAME/$FDO_DISTRIBUTION_VERSION
            --exclude-tag $FDO_DISTRIBUTION_TAG
  dependencies: []
  allow_failure: true
  only:
    - schedules

{% for distro in distributions %}
{% for version in distro.versions %}
{{distro.name}}:{{version}}@container-clean:
  extends:
    - .container-clean
  variables:
    GIT_STRATEGY: none
    CURRENT_CONTAINER_IMAGE: $CI_REGISTRY_IMAGE/{{distro.name}}/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG

{% endfor %}
{% endfor %}

#################################################################
#                                                               #
#                       build stage                             #
#                                                               #
#################################################################

.build@template:
  script:
    - env
    - r=0
    - .gitlab-ci/run-test.sh 2>&1 | tee /tmp/nm-test.log || r=$?
    - mv /tmp/nm-test.log .
    - exit $r
  dependencies: []


#################################################################
#                                                               #
#                          test stage                           #
#                                                               #
#################################################################

{% for distro in distributions %}
{% for version in distro.versions %}

t_{{distro.name}}:{{version}}:
  extends:
    - .build@template
    - .fdo.distribution-image@{{distro.name}}
{% if distro == default_distro %}
    - .nm_artifacts
{% else %}
    - .nm_artifacts_debug
{% endif %}
  stage: tier{{distro.tier}}
{% if distro.tier <= 1 %}
  parallel:
    matrix:
      - NM_TEST_SELECT_RUN:
        - autotools+gcc+docs+valgrind
        - meson+gcc+docs+valgrind
        - autotools+clang
        - meson+clang
        - rpm+autotools
        - rpm+meson
        - tarball
        - subtree
{% endif %}
  variables:
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
    {# Where we have extra_variables defined, add them to the list #}
    {% if distro.build is defined and distro.build.extra_variables is defined %}
    {% for var in distro.build.extra_variables %}
    {{var}}
    {% endfor %}
    {% endif %}
  needs:
    - "tier{{distro.tier}}:{{distro.name}}:{{version}}@prep"
{% endfor %}
{% endfor %}

#################################################################
#                                                               #
#                      specific jobs                            #
#                                                               #
#################################################################

check-patch:
  extends:
    - .fdo.distribution-image@{{default_distro.name}}
  variables:
    FDO_DISTRIBUTION_VERSION: '{{default_distro.versions[0]}}'
    FDO_DISTRIBUTION_TAG: ${{default_distro.name.upper()}}_TAG
  needs:
    - "tier{{default_distro.tier}}:{{default_distro.name}}:{{default_distro.versions[0]}}@prep"
  stage: tier1
  script:
    - date '+%Y%m%d-%H%M%S'; NM_CHECKPATCH_FETCH_UPSTREAM=1 contrib/scripts/checkpatch-feature-branch.sh
  allow_failure: true

check-tree:
  extends:
    - .fdo.distribution-image@{{default_distro.name}}
  variables:
    FDO_DISTRIBUTION_VERSION: '{{default_distro.versions[0]}}'
    FDO_DISTRIBUTION_TAG: ${{default_distro.name.upper()}}_TAG
  needs:
    - "tier{{default_distro.tier}}:{{default_distro.name}}:{{default_distro.versions[0]}}@prep"
  stage: tier1
  script:
    - date '+%Y%m%d-%H%M%S'; contrib/scripts/nm-python-black-format.sh --check
    - date '+%Y%m%d-%H%M%S'; git ls-files -z -- 'po/*.po' | xargs -0 -n1 msgfmt -vc
    - date '+%Y%m%d-%H%M%S'; contrib/scripts/nm-code-format.sh -n
    - date '+%Y%m%d-%H%M%S'; ci-fairy generate-template && git diff --exit-code

pages:
  stage: deploy
  script:
    - mv docs-html public
  artifacts:
    expire_in: 20 days
    paths:
      - public
  only:
    - main
  dependencies:
    - "t_{{default_distro.name}}:{{default_distro.versions[0]}}: [autotools+gcc+docs+valgrind]"
  needs:
    - "t_{{default_distro.name}}:{{default_distro.versions[0]}}: [autotools+gcc+docs+valgrind]"

triage:issues:
  stage: triage
  image: ruby:2.7
  script:
    - gem install gitlab-triage
    - gitlab-triage -d --token $API_TOKEN --source-id $SOURCE_ID
  only:
    - schedules

# Have detached MR pipeline (https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html)
# https://gitlab.freedesktop.org/freedesktop/freedesktop/-/issues/540#what-it-means-for-me-a-maintainer-of-a-project-part-of-gitlabfreedesktoporg
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_PIPELINE_SOURCE == 'push'
